console.group("Topic: DOM");

// Task 01
// Найти элемент с id= "t01". Вывести в консоль.
// Найти родительский элемент и вывести в консоль.
// Найти дочерние ноды, если они есть, и вывести в консоль
// названия и тип нод.
{
    const t01 = document.querySelector('#t01');
    console.log(t01);
    console.log(t01.parentNode);
    t01.childNodes.forEach(item => console.log(`${item.nodeName} : ${item.nodeType}`));
}
// Task 02
// Подсчитать количество <li> элементов на странице. Для поиска элементов использовать
// getElementsByTagName(). Вывести в консоль.
// Добавить еще один элемент в список и вывести снова их количество.
{
    let countLi = document.getElementsByTagName('li').length;
    console.log(countLi);
    const li = document.createElement('li');
    const ul = document.querySelector('#t01 ul')
    ul.appendChild(li);
    countLi = document.getElementsByTagName('li').length;
    console.log(countLi);
}
// Task 03
// Получить элементы <li> используя метод querySelectorAll() и вывети их в консоль
// Добавить новый <li> и снова вывести в консоль
{
    let countLi = document.querySelectorAll('li').length;
    console.log(countLi);
    const li = document.createElement('li');
    const ul = document.querySelector('#t01 ul')
    ul.appendChild(li);
    countLi = document.getElementsByTagName('li').length;
    console.log(countLi);
}
// Task 04
// Найти все первые параграфы в каждом диве и установить цвет фона #ffff00
{
    const allParagraph = document.querySelectorAll('div p');
    allParagraph.forEach(p => p.style.backgroundColor = "#ffff00");
}
// Task 05
// Подсчитать сумму строки в таблице и вывести ее в последнюю ячейку
{
    const table = document.querySelectorAll('td');
    let values = []; 
    table.forEach(item => values.push(item.innerText));
    values = values.map(item => Number(item));
    let sum = 0;
    values.forEach(item => sum += item);
    document.querySelector('td:last-of-type').innerHTML = sum;
}
// Task 06
// Вывести значения всех атрибутов элемента с идентификатором t06
{
    const t06 = document.querySelector('#t06');
    console.log(t06);
}
// Task 07
// Получить объект, который описывает стили, которые применены к элементу на странице
// Вывести объект в консоль. Использовать window.getComputedStyle().
{
    const h5 = document.querySelector('h5');
    console.log(window.getComputedStyle(h5));
}
// Task 08
// Установите в качестве контента элемента с идентификатором t08 следующий параграф
// <p>This is a paragraph</>
{
    const t08 = document.querySelector('#t08');
    t08.innerHTML = t08.innerHTML + "<p>This is a paragraph</>";
}
// Task 09
// Создайте элемент <div class='c09' data-class='c09'> с некоторым текстовым контентом, который получить от пользователя,
// с помощью prompt, перед элементом с идентификатором t08,
// когда пользователь кликает на нем
{
    const button = document.querySelector('button:last-child');
    console.log(button);
    button.addEventListener('click', () => {
        const lastSection = document.querySelector('section:last-of-type');
        const div = document.createElement('div');
        div.setAttribute('class', 'c09');
        div.setAttribute('data-class', 'c09');
        div.innerHTML = prompt('Введите что-нибудь');
        const t08 = document.querySelector('#t08');
        lastSection.insertBefore(div, t08);
    })
}
// Task 10
// Удалите у элемента с идентификатором t06 атрибут role
// Удалите кнопку с идентификатором btn, когда пользователь кликает по ней
{
    const t06 = document.querySelector('#t06');
    t06.removeAttribute('role');
    const button = document.querySelector('#btn');
    button.addEventListener('click', () => {
        button.parentNode.removeChild(button);
    })
}
console.groupEnd();
