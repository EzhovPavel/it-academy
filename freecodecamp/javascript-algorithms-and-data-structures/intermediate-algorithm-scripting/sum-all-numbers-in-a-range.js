function sumAll([first, second]) {
  let flag;
  if(first - second > 0) {
    flag = -1;
  } else {
    flag = 1;
  }
  if(first !== second) {
    return first + sumAll([first + flag, second]);
  } else { 
    return first;
  }
}
console.log(sumAll([1, 4]));
