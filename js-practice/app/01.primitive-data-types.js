console.group("Topic: Primitive Data Types");

// Task 01
// Объявите переменную days и проинициализируйте ее числом от 1 до 10.
// Преобразуйте это число в количество секунд и выведите в консоль.
{
    const days = Math.round((Math.random() * 11));
    const second = days * 24 * 60 * 60;
    console.log(second);
}
// Task 02
// Объявите две переменные: admin и name. Установите значение переменной name
// в ваше имя. Скопируйте это значение в переменную admin и выведите его в консоль.
{
    const name = 'Pavel Ezhov';
    const admin = name;
    console.log(admin);
}
// Task 03
// Объявите три переменных: a, b, c. Присвойте им следующие значения: 10, 2, 5.
// Объявите переменную result1 и вычислите сумму значений переменных a, b, c.
// Объявите переменную min и вычислите минимальное значение переменных a, b, c.
// Выведите результат в консоль.
{
    const a = 10;
    const b = 2;
    const c = 5;
    const result1 = a + b + c;
    const min = Math.min(a, b, c);
    console.log(result1, min);
}
// Task 04
// Объявите три переменных: hour, minute, second. Присвойте им следующие значения:
// 10, 40, 25. Выведите в консоль время в формате 10:40:25.
{
    const hour = 10;
    const minute = 40;
    const second = 25
    console.log(`${hour}:${minute}:${second}`);
}
// Task 05
// Объявите переменную minute и проинициализируйте ее целым числом.
// Вычислите к какой четверти принадлежит это число и выведите в консоль.
{
    let minute = 31;
    minute /= 15;
    if(minute <= 1) {
        console.log('Первая четверть');
    } else if(minute > 1 && minute <= 2) {
        console.log('Вторая четверть');
    } else if(minute > 2 && minute <= 3) {
        console.log('Третья четверть');
    } else {
        console.log('Четвертая четверть');
    } 
}
// Task 06
// Объявите две переменные, которые содержат стоимость товаров:
// первый товар - 0.10 USD, второй - 0.20 USD
// Вычислите сумму и выведите в консоль. Используйте toFixed()
{
    const product1 = '0.10 USD';
    const product2 = '0.20 USD';

    console.log(Number(product1 + product2));
}
// Task 07
// Объявите переменную a.
// Если значение переменной равно 0, выведите в консоль "True", иначе "False".
// Проверьте, что будет появляться в консоли для значений 1, 0, -3.
{
    const a = -3; 
    console.log(a === 0);
}
// Task 08
// Объявите две переменных: a, b. Вычислите их сумму и присвойте переменной result.
// Если результат больше 5, выведите его в консоль, иначе умножте его на 10
// и выведите в консоль.
// Данные для тестирования: 2, 5 и 3, 1.
{
    const a = 1;
    const b = 3;
    const result = a + b;
    result > 5 ? console.log(result) : console.log(result * 10);
}
// Task 09
// Объявите переменную month и проинициализируйте ее числом от 1 до 12.
// Вычислите время года и выведите его в консоль.
{
    const month = Math.round((Math.random() * 13));
    switch(month) {
        case 12: 
        case 1:
        case 2: 
            console.log('Зима');
            break;
        case 3:
        case 4:
        case 5: 
            console.log('Весна');
            break;
        case 6: 
        case 7: 
        case 8:
            console.log('Лето');
            break;
        case 9:
        case 10:
        case 11:
            console.log('Осень');
            break;
    }
}
// Task 10
// Выведите в консоль все числа от 1 до 10.
{
    for(let i = 1; i <= 10; i++) {
        console.log(i);
    }
}
// Task 11
// Выведите в консоль все четные числа от 1 до 15.
{
    for(let i = 2; i <= 14; i += 2) {
        console.log(i);
    }
}
// Task 12
// Нарисуйте в консоле пирамиду на 10 уровней как показано ниже
// x
// xx
// xxx
// xxxx
// ...
{
    let pyramid = '';
    for(let i = 1; i <= 10; i++) {
        pyramid += 'x';
        console.log(pyramid);
    } 
}
// Task 13
// Нарисуйте в консоле пирамиду на 9 уровней как показано ниже
// 1
// 22
// 333
// 4444
// ...
{
    for(let i = 1; i <= 9; i++){
        let pyramid = '';
        for(let j = 1; j <= i; j++) {
            pyramid += i;
        }
        console.log(pyramid);
    }
}
// Task 14
// Запросите у пользователя какое либо значение и выведите его в консоль.
{
    //const value = prompt('Введите значение');
    //console.log(value);
}
// Task 15
// Перепишите if используя тернарный опертор
// if (a + b < 4) {
//   result = 'Мало';
// } else {
//   result = 'Много';
// }
{
    const a = 1;
    const b = 3;
    const result = (a + b) < 4 ? 'Мало' : 'Много';
}
// Task 16
// Перепишите if..else используя несколько тернарных операторов.
// var message;
// if (login == 'Вася') {
//   message = 'Привет';
// } else if (login == 'Директор') {
//   message = 'Здравствуйте';
// } else if (login == '') {
//   message = 'Нет логина';
// } else {
//   message = '';
// }
{
    let message;
    const login = 'Вася';
    login === 'Вася' ? message = 'Привет' : 
        login === 'Директор' ? message = 'Здравствуйте' : 
        login === '' ? message = 'Нет логина' : 
        message = '';
}
// Task 17
// Замените for на while без изменения поведения цикла
// for (var i = 0; i < 3; i++) {
//   alert( "номер " + i + "!" );
// }
{
    let i = 0;
    while(i < 3) {
        //alert( "номер " + i + "!" );
        i++;
    }
}
// Task 18
// Напишите цикл, который предлагает prompt ввести число, большее 100.
// Если пользователь ввёл другое число – попросить ввести ещё раз, и так далее.
// Цикл должен спрашивать число пока либо посетитель не введёт число,
// большее 100, либо не нажмёт кнопку Cancel (ESC).
// Предусматривать обработку нечисловых строк в этой задаче необязательно.
{
    let number = 0;
    // do {
    //     //number = prompt('Введите число больше 100');
    //     console.log(number);
    // }
    // while(number < 100 && number !== null);
}
// Task 19
// Переписать следующий код используя switch
// var a = +prompt('a?', '');
// if (a == 0) {
//   alert( 0 );
// }
// if (a == 1) {
//   alert( 1 );
// }
// if (a == 2 || a == 3) {
//   alert( '2,3' );
// }
{
    // const a = +prompt('a?', '');
    // switch(a) {
    //     case 0: 
    //         alert(0);
    //         break;
    //     case 1: 
    //         alert(1);
    //         break;
    //     case 2:
    //     case 3:
    //         alert('2,3');
    //         break;
    // }
}
// Task 20
// Объявите переменную и проинициализируйте ее строчным значением в переменном
// регистре. (Например так "таООооОддОО")
// Напишите код, который преобразует эту строку к виду:
// первая буква в верхнем регистре, остальные буквы в нижнем регистре.
// Выведите результат работы в консоль
// Используйте: toUpperCase/toLowerCase, slice.
{
    let str = 'таООооОддОО';
    str = str.slice(0, 1).toUpperCase() + str.slice(1).toLowerCase();
    console.log(str);
}

// Task 21
// Напишите код, который выводит в консоль true, если строка str содержит
// „viagra“ или „XXX“, а иначе false.
// Тестовые данные: 'buy ViAgRA now', 'free xxxxx'
{
    const str = 'free xxxxx';
    console.log(str.indexOf('viagra') !== -1 || str.indexOf('XXX') !== -1)
}
// Task 22
// Напишите код, который проверяет длину строки str, и если она превосходит
// maxlength – заменяет конец str на "...", так чтобы ее длина стала равна maxlength.
// Результатом должна быть (при необходимости) усечённая строка.
// Выведите строку в консоль
// Тестовые данные:
//  "Вот, что мне хотелось бы сказать на эту тему:", 20
//  "Всем привет!", 20
{
    let str = 'Вот, что мне хотелось бы сказать на эту тему:';
    const maxlength = 17;
    if(str.length > 20) {
        str = `${str.slice(0, maxlength)}...`;
    }
    console.log(str);
}

// Task 23
// Напишите код, который из строки $100 получит число и выведите его в консоль.
{
    console.log(Number('$100'.slice(1)));
}
//
// Task 24
// Напишите код, который проверит, является ли переменная промисом
console.groupEnd();
