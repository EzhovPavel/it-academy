console.group("Topic: Arrays");

// Task 01
// RU: Создать массив. Получить последний элемент массива.
//     1.    Без удаления этого элемента из массива.
//     2.    С удалением этого элемента из массива
//    Выведите массивы в консоль
// EN: Create an array of any elements. Get the last element from this array.
//     1.    without deleting this element from an array;
//     2.    with deleting this element from an array.
//     Display them in the console.
const mass = [1,2,3,4,5,6,7,8,9,10];
const lastElem1 = mass.reverse()[0];
//console.log(lastElem1);
mass.reverse();
const lastElem2 = mass.pop();
//console.log(lastElem2);
// Task 02
// RU: Создать массив любых элементов. Добавить элемент в конец массива.
//     1. Модифицировать текущий массив
//     2. Создать новый массив
//     Выведите массивы в консоль
// EN: Create an array of any elements. Add new element to the end of this array.
//     1. mutate current array;
//     2. create a new array.
//     Disply them in the conole.
const mass1 = [1,2,3,4,5,6,7,8,9,10];
mass1.push(11);
// console.log(mass1);
const mass2 = mass1;
mass2.push(12);
//console.log(mass2);
// Task 03
// RU: Создать массив любых элементов. Вставить новый элемент под индексом 3.
//     1. Модифицировать текущий массив
//     2. Создать новый массив
//     Выведите массивы в консоль
// EN: Create an array of any elements. Insert a new element with index 3.
//     1. mutate current array;
//     2. create a new array.
//     Disply them in the conole.
const mass3 = [1,2,3,4,5,6,7,8,9,10];
mass3.splice(3, 0, 111);
//console.log(mass3);
const mass4 = mass3;
mass4.splice(3, 0, 111);
// console.log(mass4);
// Task 04
// RU: Создать массив любых элементов.
//     Обойти элементы массива и вывести их в консоль.
// EN: Create an array of any elements.
//     Iterate over this array and display each element in the console.
const mass5 = [1,2,3,4,5,6,7,8,9,10];
//mass5.forEach(item => console.log(item));
// Task 05
// RU: Создать массив чисел в диапазоне от 0 до 100.
//     Подсчитать и вывети сумму тех элементов, которые больше 50.
// EN: Create an array of numbers in the range from 0 to 100.
//     Calculate and display the sum of the elements, which are greater than 50.
// const mass6 = [];
// for(let i = 90; i <= 100; i++) {
//     mass6.push(i);
// }
// const mass7 = mass6.filter(item => item > 50).reduce((accum, item) => accum + item);
//console.log(mass7);
// Task 06
// RU: Создать массив строк. На основе этого массива создать строку –
//     объдинить все элементы массива, используя определенный разделитель.
// EN: Create an array of strings. Create a string on the basis of this array.
//     This string should contain all elements from an array separated by certain delimeter.
const mass8 = ['Str', 'Air', 'Golp', '123'];
const str = mass8.join('-');
//console.log(str);
// Task 07
// RU: Создать массив чисел от 1 до 20 в случайном порядке.
//     Отcортировать массив по возрастанию. Вывести его в консоль.
//     Получить массив, отсортрованный в обратном порядке, и вывести его в консоль.
// EN: Create an array of numbers in the range from 1 to 20 in random order.
//     Sort this array in ascending order. Display it in the console.
//     Create a copy of the previous array in reverse order. Disply it in the console.
let mass9 = [];
for(let i = 0; i <= 20; i++) {
    mass9.push(Math.floor(Math.random() * (20 - 1) + 1));
}
//console.log(mass9);
mass9.sort((a, b) => a - b);
//console.log(mass9);
mass9.sort((a, b) => b - a);
//console.log(mass9);
// Task 08
// RU: Создать массив [3, 0, -1, 12, -2, -4, 0, 7, 2]
//     На его основе создать новый массив [-1, -2, -4, 0, 0, 3, 12, 7, 2].
//     первая часть - отрицательные числа в том же порядке
//     вторая часть - нули
//     третья часть - положительные числа в том же порядке.
// EN: Create the array: [3, 0, -1, 12, -2, -4, 0, 7, 2]
//     Use this array and create new one: [-1, -2, -4, 0, 0, 3, 12, 7, 2].
//     First part - negative values from the original array in the same order,
//     Next part - zeroes
//     Last part - positive values from the original array in the same order.
const mass10 = [3, 0, -1, 12, -2, -4, 0, 7, 2];
const mass11 = mass10.slice().sort((a, b) => a - b);
//console.log(mass11);
// Task 09
// RU: 1. Создайте массив styles с элементами "Jazz", "Blues".
//     2. Добавьте в конец значение "Rock-n-Roll".
//     3. Замените предпоследнее значение с конца на "Classics".
//     4. Удалите первый элемент из массива и выведите его в консоль.
//     5. Добавьте в начало два элемента со значениями "Rap" и "Reggae".
//     6. Выведите массив в консоль.
// EN: 1. Create an array styles with two elements "Jazz", "Blues".
//     2. Add new element "Rock-n-Roll" to the end of the array.
//     3. Replace the last but one element with new value "Classics".
//     4. Remove the first element from the array and disply it in the console.
//     5. Add two new elements "Rap" and "Reggae" at the begining of the array.
//     6. Display an array in the console.
const styles = ['Jazz', 'Blues'];
styles.push('Rock-n-Roll');
styles[styles.length - 1] = 'Classics';
//console.log(styles.shift());
styles.unshift('Rap', 'Reggae');
//console.log(styles);
// Task 10
// RU: Подсчитать в строке "dskjdhfkjshdfkjhsdkjureyteiruyiqywehjkh"
//     отдельно количество букв r, k, t и вывести в консоль.
// EN: Calculate the number of letters r, k, t in this string
//     "dskjdhfkjshdfkjhsdkjureyteiruyiqywehjkh" and display them in the console.
const str1 = 'dskjdhfkjshdfkjhsdkjureyteiruyiqywehjkh';
const counterR = str1.split('r').length - 1;
const counterK = str1.split('k').length - 1;
const counterT = str1.split('t').length - 1;
//console.log(counterR, counterK, counterT);
// Task 11
// RU: Создать массив любых элементов.
//     Получить случайный элемент из массива и вывести его в консоль.
// EN: Create an array of any elements.
//     Get the random element from this array and display it in the console.
const mass12 = [1,2,3,4,5,6,7,8,9,10];
//console.log(mass12[Math.floor(Math.random() * mass12.length)]);
// Task 12
// RU: Создать двумерный массив:
//     1 2 3
//     4 5 6
//     7 8 9
//     Вывести его в консоль.
// EN: Create two-dementional array:
//     1 2 3
//     4 5 6
//     7 8 9
//     Display it in the console.
let mass13 = [
    [1,2,3],
    [4,5,6],
    [7,8,9]
];
let str2 = '';
for(let i = 0; i < 3; i++) {
    for(let j = 0; j < 3; j++) {
        str2 += mass13[i][j];
    }
    str2 += '\n';
}
//console.log(str2);
// Task 13
// RU: Преобразовать массив из предыдущего задания в одномерный.
//     Вывести его в консоль
// EN: Transform an array from the previous task into one-dementional array.
//     Display it in the console.
mass13 = mass13.flat(1);
//console.log(mass13);
// Task 14
// RU: Создать массив любых элементов.
//     На его основе получить новый массив – подмножество элементов
//     оригинального массива начиная с индекса a и заканчивая индексом b.
//     Вывести массив в консоль.
// EN: Create an array of any elements.
//     Create new one on the basis of the originl array. New array should
//     contain elements from index a to index b.
//     Display it in the console.
const mass14 = [1,2,3,4,5,6,7,8,9,10];
// Task 15
// RU: Создать массив любых элементов.
//     Найти индекс указаного элемента в массиве и вывести его в консоль.
// EN: Create an array of any elements.
//     Find the index of a particular element in the array and disply it in the console.
const mass15 = [1,2,3,4,5,6,7,8,9,10];
//console.log(mass15.indexOf(5));
// Task 16
// RU: Создать массив с дублями элементов. На его основе создать новый массив
//     уникальных элементов (удалить дубли).
//     Вывести новый массив в консоль.
// EN: Create an array with duplicate elements. Create new one on the basis of the originl array.
//     Remove duplicated elements.
//     Display it in the console.
const mass16 = [1,1,2,2,3,3,4,4,5,5,5,6,1,1,1,7,7,8,9,10];
const mass17 = mass16.filter((item, pos) => mass16.indexOf(item) === pos);
// Второй вариант
const mass18 = Array.from(new Set(mass16));
//console.log(mass17);
//console.log(mass18);
// Task 17
// RU: Создать массив с дублями. Найти первый элемент, который дублируется.
//     Заменить этот элемент и все его копии на символ '*'.
//     Вывести массив в консоль.
// EN: Create an array with duplicate elements. Find first duplicated element.
//     Replace this element and all its copies with symbol '*'.
//     Display it in the console.
//const mass19 = [1,1,2,2,3,3,4,4,5,5,5,6,1,1,1,7,7,8,9,10];

// Task 18
// RU: Создать массив целых чисел. На его основе создать массивы – представления
//     этих же чисел в бинарном, восьмеричном и шестнадцатеричном виде.
//     Вывести эти массивы в консоль.
// EN: Create an array of integer numbers. Create 3 new ones on the basis of the originl array.
//     First array contains the binary representation of the elements from the original array.
//     Second array contains the octal representation of the elements from the original array.
//     Third array contains the hexadecimal representation of the elements from the original array.
//     Display them in the console.
const mass20 = [41,23,623,21,233,444,21,54,9,10];
const mass21 = mass20.map(item => parseInt(item, 2));
//console.log(mass21);
const mass22 = mass20.map(item => parseInt(item, 8));
//console.log(mass22);
const mass23 = mass20.map(item => parseInt(item, 16));
//console.log(mass23);
// Task 19
// RU: Получить из строки 'a big brown fox jumps over the lazy dog' массив слов,
//     который содержит элементы, длина которых не больше 3 символа.
//     Вывести массив в консоль.
// EN: Get the array of words from the string 'a big brown fox jumps over the lazy dog'.
//     This array should contain only words, the length of which is 3 or less characters.
//     Display it in the console.
const str3 = 'a big brown fox jumps over the lazy dog';
const mass24 = str3.split(' ').filter(item => item.length <= 3);
//console.log(mass24);
// Task 20
// RU: Создать массив, который содержит строки и числа.
//     Проверить, содержит ли массив только строки.
//     Вывети результат в консоль
// EN: Create an array of numbers and strings.
//     Check whether this array contains only strings.
//     Display the result in the console.
const mass25 = ['a', 'b', 'c'];
//console.log(mass25.every(item => typeof item === 'string'));
// Task 21
// RU: Создать отсортированный массив чисел.
//     Реализовать функцию binarySearch(arr, value), которая принимает массив
//     и значение и возвращает индекс значения в массиве или -1.
//     Функция должна использовать бинарный поиск.
//     Вывести результат в консоль.
// EN: Create an array of numbers in sort order.
//     Implement function binarySearch(arr, value), which takes an array
//     and a value and returns the index of this value in the array or -1.
//     Function should use binary search.
//     Display the result in the console.
function binarySearch(arr,value) {
    let i = 0;
    let j = arr.length - 1;
    let h;
    while(i <= j) {
        h = Math.floor((i + j) / 2);
        if(value === arr[h]){
            return h;
        } else {
            if(value < arr[h]) {
                j = h - 1;
            } else {
                i = h + 1;
            }
        }
    }
    return -1;
}
const mass26 = [1,2,3,4,5,6,7,8,9,10];
//console.log(binarySearch(mass26, 8));
console.groupEnd();
