// the global variable
var fixedValue = 4;

// Add your code below this line
function incrementer (inc) {
  return ++inc;
  // Add your code above this line
}

var newValue = incrementer(fixedValue); // Should equal 5
console.log(fixedValue); // Should print 4
