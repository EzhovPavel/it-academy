console.group("Topic: Date object");

// Task 1
// RU: Создать текущую дату и вывести ее в формате dd.mm.yyyy и dd Month yyyy
// EN: Create current date and display it in the console according to the format
//     dd.mm.yyyy и dd Month yyyy
{
    const date = new Date(2020, 02, 10);
    console.log(`${date.getDate()}.${date.getMonth()}.${date.getFullYear()}`);
    console.log(`${date.getDate()} ${date.getMonth()} ${date.getFullYear()}`);
}
// Task 2
// RU: Создать объект Date из строки '15.03.2025'.
// EN: Create an object Date from the string '15.03.2025'.
{   
    const str = new Date('15.03.2025'.split('.').reverse().join('-'));
}
// Task 3
// RU: Создать объект Date, который содержит:
//     1. завтрашнюю дату,
//     2. первое число текущего месяца,
//     3. последнее число текущего месяца
// EN: Create an object Date, which represents:
//     1. tomorrow
//     2. first day of the current month
//     3. last day of the current month
{
    const date = {
        tomorrow: new Date(Date.now() + (1000 * 60 * 60 * 24)),
        firstDayOfMonth: new Date(),
        lastDayOfMonth: new Date()
    }
    date.firstDayOfMonth = new Date(date.firstDayOfMonth.getFullYear(), 
    date.firstDayOfMonth.getMonth() + 1, 1).getDate();
    date.lastDayOfMonth = new Date(date.lastDayOfMonth.getFullYear(), 
        date.lastDayOfMonth.getMonth() + 1, 0).getDate();
}
// Task 4
// RU: Подсчитать время суммирования чисел от 1 до 1000.
// EN: Calculate the time of summing numbers from 1 to 1000.
{
    const timeNow = Date.now();
    let sum = 0;
    for(let i = 1; i < 1000; i++){
        sum += i;
    }
    const timeBefore = Date.now();
    const timeSpent = timeBefore - timeNow;
}
// Task 5
// RU: Подсчитать количество дней с текущей даты до Нового года.
// EN: Calculate the number of days from the current date to the New Year.
{
    const dateToNewYear = Date.parse(2021, 01, 01) - Date.now();
}

console.groupEnd();
