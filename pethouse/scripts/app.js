initJqueryButtons();
$('.datepicker').datepicker({
  dateFormat: "yy-mm-dd"
});
$('form').on('submit', onFormRegisterPetSubmit); 

function initJqueryButtons() {
  $('button').button();
}

function getFormRegisterData(formEl) {
  const form = formEl.elements;
  const petName = form.petName.value;
  const ownerName = form.ownerName.value;
  const date = form.date.value;
  const time = form.time.value;
  const notes = form.notes.value;

  return {
    petName,
    ownerName,
    date,
    time, 
    notes
  };

}
function onFormRegisterPetSubmit(event) {
  event.preventDefault();
  
  const requestOptions = {
    method: 'POST',
    data: getFormRegisterData($(this).context)
  }
  const registerPetUrl = 'https://jsonplaceholder.typicode.com/todos';

  $.ajax(registerPetUrl, requestOptions).success(addPetToCommonTable);
}

function addPetToCommonTable(pet) {
  const petWithId = { ...pet, id: Date.now() };
  const petRow = getTrWithActions(petWithId);
  const deleteButton = $(petRow).find('[data-action="delete"]');

  $(deleteButton).on('click', function(e) {
    const tr = $(this).closest('tr');
    const rowId = $(tr).data('id');
    fetch('https://jsonplaceholder.typicode.com/todos' + rowId, {
      method: 'POST'
    })
      .then(() => {
        $(tr).remove();
      });
  });

  $('.pets').append(petRow);
  initJqueryButtons();
}

function getTd(content) {
  const td = document.createElement('td');

  if(typeof content === 'string') {
    td.textContent = content;
  } else if(content instanceof DocumentFragment) {
    td.append(content);
  }
  
  return td;
}

function getTrWithActions(pet) {
  const baseTr = getTr(pet);

  const buttonsFragment = document.createDocumentFragment();

  const buttonDelete = document.createElement('button');
  const iconDelete = document.createElement('i');
  $(iconDelete).addClass('fa fa-trash text-danger');
  buttonDelete.append(iconDelete);  
  $(buttonDelete).attr('data-action', 'delete');

  const buttonEdit = document.createElement('button');
  buttonEdit.textContent = 'Edit';
  $(buttonEdit).attr('data-action', 'edit');

  buttonsFragment.append(buttonEdit, buttonDelete);

  const actionsTd = getTd(buttonsFragment);

  baseTr.append(actionsTd);

  return baseTr;
}

function getTr(pet) {
  const tr = document.createElement('tr');

  const petNameTd = getTd(pet.petName);
  const ownerNameTd = getTd(pet.ownerName);
  const dateTd = getTd(pet.date);
  const timeTd = getTd(pet.time);
  const notesTd = getTd(pet.notes);

  tr.append(petNameTd, ownerNameTd, dateTd, timeTd, notesTd);
  tr.setAttribute('data-id', pet.id);

  return tr;
}