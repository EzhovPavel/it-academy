const squareList = (arr) => {
    // only change code below this line
    arr = arr.filter(item => item > 0 && (item ^ 0) === item);
    arr = arr.map(item => item * item);
    return arr;
    // only change code above this line
  };
  
  // test your code
  const squaredIntegers = squareList([-3, 4.8, 5, 3, -3.2]);
  console.log(squaredIntegers);
  